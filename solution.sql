insert into users(email, password, datetime_created)
values('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00');

insert into users(email, password, datetime_created)
values('juandelacruz@gmail.com', 'passwordB', '2021-01-01 02:00:00');

insert into users(email, password, datetime_created)
values('janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00');

insert into users(email, password, datetime_created)
values('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00');

insert into users(email, password, datetime_created)
values('johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');



insert into posts(author_id, title, content, datetime_posted)
values(1, 'First Code', 'Hello World!', '2021-01-01 01:00:00');

insert into posts(author_id, title, content, datetime_posted)
values(1, 'Second Code', 'Hello Earth!', '2021-01-01 02:00:00');

insert into posts(author_id, title, content, datetime_posted)
values(2, 'Third Code', 'Welcome to Mars!', '2021-01-01 03:00:00');

insert into posts(author_id, title, content, datetime_posted)
values(4, 'Fourth Code', 'Bye bye solar system!', '2021-01-01 04:00:00');


/*1. Get all the post with an Author ID of 1*/
select * From posts
where author_id = 1;

/*2. Get all the user's email and datetime of creation*/
select email, datetime_created From users;

/*3. Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID*/
update posts
set content = 'Hello to the people of the Earth!'
where  id = 2;

/*Delete the user with an email of "johndoe@gmail.com"*/
delete from users
where email = 'johndoe@gmail.com';